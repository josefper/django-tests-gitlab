from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.


def index(request):
    return HttpResponse("Hola, esta es tu aplicacion calc")


def suma(request, op1, op2):
    return HttpResponse("Suma: {}".format(op1 + op2))


def resta(request, op1, op2):
    return HttpResponse("Resta: {}".format(op1 - op2))
