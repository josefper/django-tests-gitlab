from django.test import TestCase, Client

from .views import Counter


class CounterTest(TestCase):

    def test_creation(self):
        """Creation works"""
        couenter = Counter()
        self.assertEqual(couenter.count, 0)

    def test_increment(self):
        """Increment works"""
        counter = Counter()
        inc = counter.increment()
        self.assertEqual(counter.count, 1)
        inc = counter.increment()
        self.assertEqual(counter.count, 2)
        self.assertEqual(inc, 2)


class CmsTest(TestCase):

    def test_index(self):
        """Check index page works"""
        c = Client()
        response = c.get('/cms/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn("<h1>Django CMS</h1>", content)

    def test_not_found(self):
        """Check not found"""
        c = Client()
        response = c.get('/cms/algoinventado')
        self.assertEqual(response.status_code, 404)

    def test_actualizar_crear_contenido(self):
        """Check create content"""

        c = Client()
        response = c.post('/cms/prueba', {'valor': "La pagina de prueba"})
        self.assertEqual(response.status_code, 200)
        response = c.get('/cms/prueba')
        self.assertEqual(response.status_code, 200)

