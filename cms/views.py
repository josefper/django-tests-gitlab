from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
import datetime

from .models import Contenido, Comentario
from django.template import loader

# Create your views here.
formulario_contenido = """
<hr>
<form action="" method="POST">
    Introduce el (nuevo) contenido para esta página:
    <input type="text" name="valor">
    <input type="submit" value="Enviar">
</form>
"""

formulario_comentario = """
<hr>
<form action="" method="POST">
    Introduce un comentario para esta página: <br>
    Título: <input type="text" name="titulo"> <br>
    Cuerpo: <input type="text" name="cuerpo">
    <input type="submit" value="Enviar">
</form>
"""


@csrf_exempt
def get_content(request, clave):
    try:
        contenido = Contenido.objects.get(clave=clave)
        comentarios = contenido.comentario_set.all()
        if request.method == "GET":
            # Preparo la respuesta
            respuesta = contenido.valor
            for comentario in comentarios:
                respuesta += ("<p><b>" + comentario.titulo + "</b><br>" + comentario.cuerpo + "<br>" +
                              comentario.fecha.strftime("%Y-%m-%d %H:%M:%S") + "</p>")
            # from cambiar Contenido
            respuesta += formulario_contenido
            return HttpResponse(respuesta)
        elif request.method == "PUT":
            return HttpResponse("Ya existe el contenido para " + clave)
        elif request.method == "POST":
            if "valor" in request.POST:
                valor = request.POST["valor"]
                contenido.valor = valor
                contenido.save()
            if "titulo" in request.POST:
                titulo = request.POST["titulo"]
                cuerpo = request.POST["cuerpo"]
                c = Contenido.objects.get(clave=clave)
                com_db = Comentario(titulo=titulo, cuerpo=cuerpo, fecha=datetime.datetime.now(), contenido=c)
                com_db.save()
            # Preparo la respuesta
            respuesta = contenido.valor
            for comentario in comentarios:
                respuesta += ("<p><b>" + comentario.titulo + "</b><br>" + comentario.cuerpo + "<br>" +
                              comentario.fecha.strftime("%Y-%m-%d %H:%M:%S") + "</p>")
            # from cambiar Contenido
            respuesta += formulario_contenido + formulario_comentario

            template = loader.get_template('contenido.html')
            contexto = {
                'contenido': contenido,
                'comentarios': comentarios
            }
            return HttpResponse(template.render(contexto, request))

    except Contenido.DoesNotExist:
        if request.method == "PUT":
            valor = request.body.decode("utf-8")
            new_contenido = Contenido(clave=clave, valor=valor)
            new_contenido.save()

            # Preparo la respuesta
            respuesta = new_contenido.valor
            comentarios = new_contenido.comentario_set.all()
            for comentario in comentarios:
                respuesta += ("<p><b>" + comentario.titulo + "</b><br>" + comentario.cuerpo + "<br>" +
                              comentario.fecha.strftime("%Y-%m-%d %H:%M:%S") + "</p>")
            # from cambiar Contenido
            respuesta += formulario_contenido + formulario_comentario

            template = loader.get_template('contenido.html')
            contexto = {
                'contenido': new_contenido,
                'comentarios': comentarios
            }
            return HttpResponse(template.render(contexto, request))
        elif request.method == "POST":
            if "valor" in request.POST:
                valor = request.POST["valor"]
                new_contenido = Contenido(clave=clave, valor=valor)
                new_contenido.save()
            if "titulo" in request.POST:
                titulo = request.POST["titulo"]
                cuerpo = request.POST["cuerpo"]
                com_db = Comentario(titulo=titulo, cuerpo=cuerpo, fecha=datetime.datetime.now())
                com_db.save()

            # Preparo la respuesta
            respuesta = new_contenido.valor
            comentarios = new_contenido.comentario_set.all()
            for comentario in comentarios:
                respuesta += ("<p><b>" + comentario.titulo + "</b><br>" + comentario.cuerpo + "<br>" +
                              comentario.fecha.strftime("%Y-%m-%d %H:%M:%S") + "</p>")
            # from cambiar Contenido
            respuesta += formulario_contenido + formulario_comentario

            template = loader.get_template('contenido.html')
            contexto = {
                'contenido': new_contenido,
                'comentarios': comentarios
            }
            return HttpResponse(template.render(contexto, request))
        else:
            raise Http404("No existe el contenido para " + clave)


def index(request):
    contenidos =  Contenido.objects.all()
    #carga la template
    template = loader.get_template('index.html')
    contexto = {
        'contenidos': contenidos,
    }
    return HttpResponse(template.render(contexto, request))


class Counter():
    def __init__(self):
        self.count: int = 0

    def increment(self):
        self.count += 1
        return self.count
